//
//  Builder.h
//  ColombiaDemo
//
//  Created by Mohit Kumar on 10/21/15.
//  Copyright © 2015 Mohit Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ColombiaAdManager.h"
#import "CLBItemDelegate.h"
#import <CoreLocation/CoreLocation.h>

@class CLBAdRequestResponse;


@interface CLBBuilder : NSObject

@property (nonatomic,strong)NSMutableArray *requestResponseSet;
@property (nonatomic,strong)NSString *referer;
@property (nonatomic,strong)NSDate * age;
@property (nonatomic,assign)ColombiaGENDER gender;
@property (nonatomic,strong)NSArray *installAdApps;
@property (nonatomic,strong)NSString *currentLocation;
@property (nonatomic,assign)BOOL downloadImage;
@property (nonatomic,assign)BOOL downloadIcon;
@property (nonatomic,strong)ColombiaAdManager *manager;

@property (nonatomic,assign)NSInteger adWidth;
@property (nonatomic,assign)NSInteger adHeight;

@property (nonatomic, assign) NSNumber *downloadAudio;
@property (nonatomic, assign) NSNumber *downloadVideo;
@property (nonatomic,assign)  BOOL autoPlayEnabled ;

-(instancetype)initWithManager:(ColombiaAdManager*)manager;
-(NSMutableSet*)getAdManagerItems;
-(NSMutableArray*)getAdRequests;
-(CLBBuilder*)addRequestWithAdUnitID:(long long)adUnitId positionId:(NSInteger)positionId sectionId:(NSString*)sectionId additionalInfo:(id)requestInfo ItemDelegate:(id<CLBItemDelegate>)ItemDelegate;

/*!
 * @discussion Return adSlot in format- AdUnitID~Section~Position
 */
-(NSString *)getAdSlot;
/*!
 * @discussion Name of referer ( generally website url is passed) returned
 */
-(NSString*)getReferer;
/*!
 * @discussion Add referer in the Ad Request. Generally need to pass the URL of your website
 * @param referer Referer string to be passed in request
 */
-(CLBBuilder*)addReferer:(NSString*)referer;
/*!
 * @discussion DOB to be passed
 * @param age DOB of user
 */
-(CLBBuilder*)addAge:(NSDate *)age;
/*!
 * @discussion Return DOB for a particualr request.
 */
-(NSDate *)getAge;
/*!
 * @discussion Add gender of current user in your app
 * @param gender Pass in format of ColombiaGender
 */
-(CLBBuilder*)addGender:(ColombiaGENDER)gender;
/*!
 * @discussion Return gender passed in request
 */
-(ColombiaGENDER)getGender;
/*!
 * @discussion For better result, add Location of your device if applicable.
 * @param location CLLocation object to be passed
 */
-(CLBBuilder*)addLocation:(CLLocation*)location;
/*!
 * @discussion Pass bool value to define weather to download banner image OR not. Ths is specially used for banner type of ads.
 * @param downloadImage used to define weather to download or not
 */
-(CLBBuilder*)downloadImageBitmap:(BOOL)downloadImage;
/*!
 * @discussion Used for Inline Video ads. Autoplay used to define weather video should play automatically after coming in frame OR user should play that manually.
 */
-(CLBBuilder*)enableVideoAutoPlay:(BOOL)autoPlay;

/*!
 * @discussion Developer can pass audience value (coma seperated) with key.
 */
-(CLBBuilder*)addCustomAudience:(NSString *)value forKey:(NSString *)key;

/*!
 * @discussion Return format type weather to use XML OR JSON. Used by SDK only.
 */
-(NSInteger)getResponseFormat;

-(NSString*)getLocation;
-(id<CLBItemDelegate>)delegate;
/*!
 * @discussion Size of banner ads.
 */
-(CLBBuilder *)addAdSize:(CGSize)size;
/*!
 * @discussion Used to define size for Audio/Audio+banner companion. SDK will match these size on priority
 */
-(CLBBuilder *)addMediaAdSize:(CGSize)size;

/*!
 * @discussion This will be used for Inline/Outstream video ads, overlay color will be used for overlay background when video is paused/completed.
 */
-(CLBBuilder *)addVideoOverlayColor:(UIColor *)overlayColor;

-(UIColor *)overlayColor;
-(NSMutableArray *)bannerSizes;
-(NSInteger)getAdWidth;
-(NSInteger)getAdHeight;
-(ColombiaAdRequest *)build;
-(BOOL)isFirstImpression;
-(BOOL)isFirstAdCall;
-(void)updateFirstImpressionValue:(BOOL)value;
-(void)updateFirstAdCallValue:(BOOL)value;
-(NSArray *)getCustomAudiences;
-(CLBBuilder *)downloadAudio:(BOOL)downloadAudio;
-(CLBBuilder *)downloadVideo:(BOOL)downloadVideo;

@end
