//
//  CLBCarouselAdView.h
//  ColombiaSDK
//
//  Created by Mohit Kumar on 11/28/16.
//  Copyright © 2016 TIL. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CLBItemResponse;

@interface CLBCarouselAdView : UIView

-(instancetype)init NS_UNAVAILABLE;

/*!
 * @discussion Create Carousel view object and return to user
 * @param itemResponse ItemResponse for which carousel need to be rendered
 * @return CarouselView object
 */
-(instancetype)initWithItemResponse:(CLBItemResponse *)itemResponse;

/*!
 * @discussion Return size for carousel.
 * @return CGSize object. It can be used while calculating height in UITableView/CollectionView/ScrollView
 */
+(CGSize)getCarouselSize;

/*!
 * @discussion Call this method after creating the view using init method of class. After commit, SDK will handle rendering of carousel.
 */
-(void)commit;

@end
