//
//  ItemDelegate.h
//  ColombiaDemo
//
//  Created by Mohit Kumar on 10/27/15.
//  Copyright © 2015 Mohit Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLBItemResponse.h"
#import "ColombiaAdItem.h"

@protocol CLBItemDelegate <NSObject>
/*!
 * @discussion This method will be called once received response from server and the response is valid
 * @param request Request object for which user received the response
 * @param response Instance of CLBItemResponse, Containing server response
 */
-(void)adRequest:(ColombiaAdRequest *)request didReceiveResponse:(CLBItemResponse *)response;

/*!
 * @discussion This will be called if server return any error OR response from server is not valid
 * @param request Request object for which user received the error
 * @param exception Exception instance contains message and ItemResponse object in userInfo.
 */
-(void)adRequest:(ColombiaAdRequest *)request didReceiveError:(NSException *)exception;

// Option are useed when user is implementing Audio/Audio+Banner/Incentive Video Ads.
@optional


/*!
 * @discussion called if user has made returnItem URL request as true. So in this case when user will tap on view, this method will call with item object and URL that need to be open. 
 
 THIS WILL BE CALLED ONLY WHEN returnItemURL is set TRUE and ITEM IS OF CONTENT TYPE.
 * @param item Item which is clicked
 * @return BOOL value to indicate weather clickthrough event need to be handled by SDK or Publisher. Return YES if need to handle by publisher else return NO.
 */
-(BOOL)onItemClicked:(id<ColombiaAdItem>)item;

/*!
 * @discussion Called when the item is displayed to user, e.g audio start playing,video is visible, banner is visible
 * @param item Return Item for which the events happened
 */
-(void)onMediaItemDisplayed:(id<ColombiaAdItem>) item;
/*!
 * @discussion Called When user tapped on Ad.
 * @param item Return Item for which the events happened
 */
-(void)onMediaItemClicked:(id<ColombiaAdItem>) item;
/*!
 * @discussion Called when User close the Ad OR Ad is closed automatically OR if any error happened
 * @param item Return Item for which the events happened
 * @param action This is of type CLB_USER_ACTION containing following values to represent event type:
 typedef enum{
 CLB_AUTO_CLOSED,
 CLB_USER_CLOSED,
 CLB_SKIPPED,
 CLB_INVALID_CONFIG,
 CLB_ERROR,
 CLB_ACTION_UNKNOWN
 }CLB_USER_ACTION;
 */
-(void)onMediaItemClosed:(id<ColombiaAdItem>) item andUserAction:(CLB_USER_ACTION)action;

/*!
 * @discussion Call when the media item is finished.
 * @param item Return Item for which the events happened
 * @param sessionDurationInMillis It returns time for AdFree session which can be used in app. Time is in seconds.
 */
-(void)onMediaItemCompleted:(id<ColombiaAdItem>) item andDuration:(NSInteger)sessionDurationInMillis;
/*!
 * @discussion Call when their is any error raised from Ad.
 * @param item Return Item for which the events happened
 * @param exception contains exception object.
 */
-(void)onMediaItemError:(id<ColombiaAdItem>) item andError:(NSException *)exception;

/*!
 * @discussion Call Skip is enabled is Ads.
 * @param item Return Item for which the events happened
 */
-(void)onMediaItemSkipEnabled:(id<ColombiaAdItem>) item;

/*!
 * @discussion Call when Intersitital/Incentivised video view is about to dismiss.
 * @param item Return Item for which the events happened
 */
-(void)willCloseMedia:(id<ColombiaAdItem>)item;

/*!
 * @discussion It will be used for Outstream video. Event will be passed to app based on user action.
 * @param event Event which is performed from user
 */
-(void)didReceiveVideoEvent:(CLB_VIDEO_EVENT)event;
@end
