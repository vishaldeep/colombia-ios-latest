//
//  ItemResponse.h
//  ColombiaDemo
//
//  Created by Mohit Kumar on 10/23/15.
//  Copyright © 2015 Mohit Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ColombiaAdRequest;
@class CLBNativeItem;
@class CLBNetworkAdUnit;

@interface CLBItemResponse : NSObject

@property (nonatomic, readwrite, assign) BOOL containsAds;



//Initialize methods
-(instancetype)initWithColombiaAdRequest:(ColombiaAdRequest*)colombiaAdRequest;

//get values
-(BOOL)isSuccessful;
-(NSException*)getException;
-(void)setException:(NSException*)exception;

-(NSMutableArray*)getOrganicItems;
-(NSMutableArray*)getPaidItems;
-(long long)getAdUnitId;
-(NSString*)getSection;
-(NSInteger)getPosition;
-(NSString*)getAdSlot;
-(NSString *)getResponseTitle;
-(id)requestInfo;
-(BOOL)isContainsAds;
-(void)addOrganicAndPaidItemsInAdManager;
-(void)parseJSONResponse:(NSDictionary *)adJson;
-(void)setRequestInfo:(id)requestInfo;
-(BOOL)isImpressed;
-(BOOL)autoPlayEnabled;
-(BOOL)isCarousel;
-(NSString *)getAdUnitIconUrl;

-(ColombiaAdRequest *)adRequest;

//json parsing
- (NSMutableArray *)AdsFromJSONResponse:(NSDictionary *)jsonResponse key:(NSString*)key isAd:(BOOL)isAd;
+ (NSMutableArray *)validDictionaryArrayForKey:(NSString *)key inJSONResponse:(NSDictionary *)jsonResponse;


-(void)addItemIdInAdManager:(NSMutableArray*)requestItems adManagerItems:(NSMutableSet*)adManagerItems;

-(void)recordItemResponseImpression;

-(NSMutableArray <CLBNetworkAdUnit *> *)getNetworkAdUnits;

-(void) setPaidItems : (NSMutableArray *)paidItems;

@end


