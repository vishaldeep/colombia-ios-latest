//
//  CLBNativeAdView.h
//  ColombiaSDK
//
//  Created by mohit.kumar on 13/06/16.
//  Copyright © 2016 TIL. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ColombiaAdItem;
@class CLBItemResponse;

@interface CLBNativeAdView : UIView


/// This property must point to the native content ad object rendered by this ad view.
@property(nonatomic, strong) CLBItemResponse *itemResponse;
@property(nonatomic, strong) id<ColombiaAdItem> nativeAdItem;

// Weak references to your ad view's asset views.
@property(nonatomic, weak) IBOutlet UIView *headlineView;
@property(nonatomic, weak) IBOutlet UIView *bodyView;
@property(nonatomic, weak) IBOutlet UIView *imageView;
@property(nonatomic, weak) IBOutlet UIView *ctaView;
@property(nonatomic, weak) IBOutlet UIView *advertiserView;

@property(nonatomic, weak) IBOutlet UIView *attributionTextView;
@property(nonatomic, weak) IBOutlet UIView *colombiaLogoView;

@property(nonatomic, weak) IBOutlet UIView *iconView;
@property(nonatomic, weak) IBOutlet UIView *priceView;
@property(nonatomic, weak) IBOutlet UIView *ratingView;
@property(nonatomic, weak) IBOutlet UIView *reviewsView;


-(void) commit;

@end
