//
//  CLBNativeFbAdView.h
//  ColombiaSDK
//
//  Created by Mohit Kumar on 9/19/18.
//  Copyright © 2018 TIL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ColombiaAdItem;
@class CLBItemResponse;
@import FBAudienceNetwork;

@interface CLBNativeFacebookAdView : UIView

/// This property must point to the native content ad object rendered by this ad view.
@property(nonatomic, strong) CLBItemResponse *itemResponse;
@property(nonatomic, strong) id<ColombiaAdItem> nativeAdItem;

// Weak references to your ad view's asset views.
@property(nonatomic, weak) IBOutlet FBAdIconView *advertiserIcon;
@property(nonatomic, weak) IBOutlet UIView *adTitle;
@property(nonatomic, weak) IBOutlet UIView *sponsoredLabel;
@property(nonatomic, weak) IBOutlet FBMediaView *adMediaView;
@property(nonatomic,weak) IBOutlet FBAdChoicesView *adchoiceView;
@property(nonatomic, weak) IBOutlet UIView *socialContext;
@property(nonatomic, weak) IBOutlet UIView *adBody;
@property(nonatomic, weak) IBOutlet UIView *ctaButton;

-(void) commit;

@end
