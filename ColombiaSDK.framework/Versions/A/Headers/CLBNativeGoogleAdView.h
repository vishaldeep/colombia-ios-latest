//
//  CLBNativeGoogleAdView.h
//  ColombiaSDK
//
//  Created by Mohit Kumar on 9/19/18.
//  Copyright © 2018 TIL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ColombiaAdItem;
@class CLBItemResponse;

@import GoogleMobileAds;

@interface CLBNativeGoogleAdView : GADUnifiedNativeAdView


/// This property must point to the native content ad object rendered by this ad view.
@property(nonatomic, strong) CLBItemResponse *itemResponse;
@property(nonatomic, strong) id<ColombiaAdItem> nativeAdItem;

-(void)commit;

@end
