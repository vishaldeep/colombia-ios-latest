//
//  CLBNativeImageBannerAdView.h
//  ColombiaSDK
//
//  Created by Mohit Kumar on 2/28/17.
//  Copyright © 2017 TIL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ColombiaAdItem;
@class CLBItemResponse;

@interface CLBNativeImageBannerAdView : UIView
/// This property must point to the native content ad object rendered by this ad view.
@property(nonatomic, strong) CLBItemResponse *itemResponse;
@property(nonatomic, strong) id<ColombiaAdItem> nativeAdItem;

// Weak references to your ad view's asset views.

@property(nonatomic, weak) IBOutlet UIView *bannerImageView;

@property(nonatomic, weak) IBOutlet UIView *headlineView;
@property(nonatomic, weak) IBOutlet UIView *bodyView;
@property(nonatomic, weak) IBOutlet UIView *imageView;
@property(nonatomic, weak) IBOutlet UIView *logoView;
@property(nonatomic, weak) IBOutlet UIView *ctaView;
@property(nonatomic, weak) IBOutlet UIView *advertiserView;
@property(nonatomic, weak) IBOutlet UIView *attributionTextView;
@property(nonatomic, weak) IBOutlet UIView *colombiaLogoView;

-(void) commit;

@end
