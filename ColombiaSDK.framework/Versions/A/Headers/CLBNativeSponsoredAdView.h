//
//  CLBNativeSponsoredAdView.h
//  ColombiaSDK
//
//  Created by mohit.kumar on 10/06/16.
//  Copyright © 2016 TIL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ColombiaAdItem;
@class CLBItemResponse;
@protocol CLBItemDelegate;

@interface CLBNativeSponsoredAdView : UIView

@property(nonatomic,weak) IBOutlet UIView *companionMediaView;
@property(nonatomic,weak) IBOutlet UIView *dismissView;

/// This property must point to the native content ad object rendered by this ad view.
@property(nonatomic, strong) CLBItemResponse *itemResponse ;
@property(nonatomic, strong) id<ColombiaAdItem> nativeAdItem;

// Weak references to your ad view's asset views.
@property(nonatomic, weak) IBOutlet UIView *headlineView;
@property(nonatomic, weak) IBOutlet UIView *bodyView;
@property(nonatomic, weak) IBOutlet UIView *imageView;
@property(nonatomic, weak) IBOutlet UIView *logoView;
@property(nonatomic, weak) IBOutlet UIView *ctaView;
@property(nonatomic, weak) IBOutlet UIView *advertiserView;

@property(nonatomic, weak) IBOutlet UIView *attributionTextView;
@property(nonatomic, weak) IBOutlet UIView *colombiaLogoView;

@property(nonatomic, weak) IBOutlet UIView *priceView;
@property(nonatomic, weak) IBOutlet UIView *ratingView;
@property(nonatomic, weak) IBOutlet UIView *reviewsView;
@property(nonatomic,weak) id <CLBItemDelegate> delegate;

/*!
 * @discussion This method to enable audio in the Sponsored View. User can set True/False as per his requirements. If it is false then audio will not played else audio will be played. By default if user don;t set, Audio play will be false
 * @param enable BOOL parameter weather to play OR not
 */
-(void)enableAudio:(BOOL)enable;

/*!
 * @discussion call this only after view design is completed and all required properties are set
 */
-(void)commit;;

@end
