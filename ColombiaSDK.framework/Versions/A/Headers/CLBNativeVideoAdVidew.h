//
//  CLBNativeVideoAdVidew.h
//  ColombiaSDK
//
//  Created by mohit.kumar on 29/02/16.
//  Copyright © 2016 TIL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ColombiaAdItem;
@class CLBItemResponse;

@interface CLBNativeVideoAdVidew : UIView


@property(nonatomic,strong) id<ColombiaAdItem> nativeItem;
@property(nonatomic,strong) CLBItemResponse *itemResponse ;


// Weak references to your ad view's asset views.
@property(nonatomic, weak) IBOutlet UIView *headlineView;
@property(nonatomic, weak) IBOutlet UIView *bodyView;
@property(nonatomic, weak) IBOutlet UIView *imageView;
@property(nonatomic, weak) IBOutlet UIView *logoView;
@property(nonatomic, weak) IBOutlet UIView *advertiserView;
@property(nonatomic, weak) IBOutlet UIView *videoView;

@property(nonatomic, weak) IBOutlet UIView *attributionTextView;
@property(nonatomic, weak) IBOutlet UIView *colombiaLogoView;


/*!
 * @discussion After user set all requered propertirs then user must need to call commit so that SDK can make sure that from now on control should be taken by SDK.
 */
-(void)commit;

/*!
 * @discussion Return Size of video view based on NativeItem. App need to manage Coordinates for view and need to resize view on App Side.
 */
-(CGSize)videoSize;

-(void)pause;

-(void)play;


@end
