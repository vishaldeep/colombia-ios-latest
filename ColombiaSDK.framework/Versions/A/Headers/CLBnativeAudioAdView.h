//
//  CLBnativeAudioAdView.h
//  ColombiaSDK
//
//  Created by Mohit Kumar on 12/7/16.
//  Copyright © 2016 TIL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ColombiaAdItem;
@class CLBItemResponse;
@protocol CLBItemDelegate;

@interface CLBnativeAudioAdView : UIView

@property(nonatomic,weak) IBOutlet UIView *companionMediaView;
@property(nonatomic,weak) IBOutlet UIView *dismissView;

/// This property must point to the native content ad object rendered by this ad view.
@property(nonatomic, strong) CLBItemResponse *itemResponse ;
@property(nonatomic, strong) id<ColombiaAdItem> nativeAdItem;

// Weak references to your ad view's asset views.
@property(nonatomic, weak) IBOutlet UIView *headlineView;
@property(nonatomic, weak) IBOutlet UIView *bodyView;
@property(nonatomic, weak) IBOutlet UIView *logoView;
@property(nonatomic, weak) IBOutlet UIView *ctaView;
@property(nonatomic, weak) IBOutlet UIView *adTimerView;
@property(nonatomic, weak) IBOutlet UIView *advertiserView;

@property(nonatomic, weak) IBOutlet UIView *attributionTextView;
@property(nonatomic, weak) IBOutlet UIView *colombiaLogoView;

@property(nonatomic,weak) id <CLBItemDelegate> delegate;


/*!
 * @discussion Call only when you need to play audio only
 */
-(void)commit;;

/*!
 * @discussion Call when you need to display image/HTML
 */
-(void)commitResources;
/*!
 * @discussion Destroy player and view
 */
-(void)destroy;

@end
