//
//  Colombia.h
//  ColombiaDemo
//
//  Created by Mohit Kumar on 11/2/15.
//  Copyright © 2015 Mohit Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ColombiaAdRequest.h"
#import "CLBItemResponse.h"
#import "ColombiaAdItem.h"

typedef enum{
    CLB_QA_ENVIRONMENT,
    CLB_PRODUCTION_ENVIRONMENT,
}CLB_BUILD_ENVIRONMENT;


@interface Colombia : NSObject


/*!
 * @discussion Call this method to initialize the SDK .
 */
+(void)initializeSettings;


/*!
 * @discussion Used to fetch the current SDK version.
 * @return NSString String contains version  number of SDK.
 */
+(NSString*)getVersion;

/*!
 * @discussion this method is called to fetch the ads from server. User need to paas ColombiaAdRequest in it and it will automatically process the request and return the response to user through Colombia delegate methods.
 * @param request It is of type ColombiaAdRequest. User need to pass it to fetch the ads.
 */
+(void)getNativeAds:(ColombiaAdRequest*)request;

/*!
 * @discussion This method is used for record impression for view visibility. If view is visible for a specific time that will come from config manager then the impresison will be record on server. You just have to pass the view which is loaded in ViewController and Item object corresponding to that view.
 * @param view view is holding reference of view which user load on the screen.
 * @param nativeItem it is object which return from Colombia SDk to display the view
 */
+(void)recordImpressionWithView:(UIView *)view andNativeItem:(id<ColombiaAdItem> )nativeItem;

/*!
 * @discussion Use this method to set the sdk environment, weather QA OR Production.
 */
+(void)setBuildEnvironment:(CLB_BUILD_ENVIRONMENT)environment;

/*!
 * @discussion Use this method to enable optout for particular session of app. By default optout will be disabled.
 */
+(void)enableOptOut;

/*!
 * @discussion Use this method to disable optout in app. By default optout will be disabled.
 */
+(void)disableOptOut;

/*!
 * @discussion Used to cancel the request. Use this method only if their is any need to cancel the request
 * @param request It is of type ColombiaAdRequest. User need to pass request object that need to be cancel.
 */
+(void)cancelRequest:(ColombiaAdRequest *)request;
@end

