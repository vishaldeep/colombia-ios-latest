//
//  Item.h
//  ColombiaDemo
//
//  Created by Mohit Kumar on 10/6/15.
//  Copyright © 2015 Mohit Kumar. All rights reserved.

#import <UIKit/UIKit.h>
#import "ColombiaAdManager.h"
@class CLBItemResponse;
@class CLBVastContainer;

@protocol ColombiaAdItem <NSObject>
@required

extern NSString * const FREE;

-(NSString*)getBodyText;

-(NSString*)getTitle;

-(NSString*)getIconUrl;

-(NSData*)getIcon;

-(NSString*)getImageUrl;

-(NSData*)getImage;

-(NSString*)getCtaText;

-(double)getStarRating;

-(BOOL)isAdItem;

-(NSString*)getBrandText;

-(float)getPrice;

-(float)getMRP;

-(NSString*)getOfferPrice;

-(NSString*)getOfferText;

-(NSArray*)getExtraImages;

-(long long)getDownloadsCount;

-(long long)getReviewsCount;

-(NSString*)getPubDate;

-(NSArray*)getImpTracker;

-(CLB_ITEM_TYPE)getItemType;


-(NSString*)getDisplayUrl;

-(NSString*)getVastXml;

-(NSInteger)getDuration;

-(long long)getViewsCount;

-(NSInteger)getPlatform;

-(NSString*)getAdAttributionIcon;

-(NSString*)getAdAttributionText;

-(NSString*)getAdAttributionUrl;

-(NSString*)getSponsoredText;

-(NSString*)getSocialContext;

-(BOOL)isInHouse;

-(BOOL)isSOVItem;

-(id) thirdPartyAd;

-(CLB_AD_NETWORK)getAdNetwork;

-(NSInteger)getLineItemId;

-(id) getDataTags;


// This is for audio to know weather followup need to show or not.
-(BOOL)showFollowUp;


@optional


/*!
 * @discussion Call this methos only if you want to use SDK UI for IncentiveVideo/Banner+Audio Ads. Also called in case of Audio Ads.
 */
-(void) show:(CLBItemResponse *)itemResponse ;


/*!
 * @discussion In case of audio Ads, User is having control of that. So user can call this method to destroy the audio player, e.g in case user press skip button.
 */

-(NSURL *)getDownloadedMediaURL;

-(void) destroy;

-(void) skip;

-(NSString *) getAdUrl;

-(NSString *)getDeepLink;

-(NSString *)getVpaidURL;

-(NSString*)getVastURL;

-(CLBVastContainer *)getVastContainer;

-(NSString *)getItemID;

-(NSInteger)getDataType;

-(BOOL)isClientSide;

-(CLBItemResponse *)getItemResponse;

-(NSString *)clientID;

-(NSString *)getCurrency;

-(NSString *)getAdChoiceImageURL;

-(NSString *)getAdChoiceClickURL;

/*!
 * @discussion Used to return size for Audio/Audio banner type ads. It return size of Image/HTML/IFRAME for a particular item.
 */
-(CGSize)getMediaSize;


@end
