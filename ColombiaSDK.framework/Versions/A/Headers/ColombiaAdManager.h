//
//  ColombiaAdManager.h
//  ColombiaDemo
//
//  Created by Mohit Kumar on 10/27/15.
//  Copyright © 2015 Mohit Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef  enum {
    CLB_UNKNOWN, // == 0 (by default)
    CLB_MALE, // == 1 (incremented by 1 from previous)
    CLB_FEMALE // == 2
}ColombiaGENDER;

typedef enum{
    CLB_PRODUCT,
    CLB_CONTENT,
    CLB_APP,
    CLB_GENERAL,
    CLB_VIDEO,
    CLB_LEADGEN,
    CLB_AUDIO = 9,
    CLB_VIDEO_INCENTIVE,
    CLB_AUDIO_BANNER,
    CLB_CONTENT_VIDEO,
    CLB_SOV,
    CLB_IMAGE_BANNER = 16,
    CLB_INTERSTITIAL_VIDEO = 17,
    UNKNOWN
}CLB_ITEM_TYPE;

typedef enum{
    CLB_FACEBOOK,
    CLB_GOOGLE,
    CLB_COLOMBIA,
    CLB_CRITIO
}CLB_AD_NETWORK;

typedef enum{
    CLB_AUTO_CLOSED = 1,
    CLB_USER_CLOSED,
    CLB_SKIPPED,
    CLB_INVALID_CONFIG,
    CLB_ERROR,
    CLB_ACTION_UNKNOWN
}CLB_USER_ACTION;


typedef enum{
    CLB_PAUSE=1,
    CLB_MUTE,
    CLB_UNMUTE
}CLB_VIDEO_EVENT;

@interface ColombiaAdManager : NSObject

@property (nonatomic,strong)NSMutableSet *ids;


-(NSMutableSet*)getItems;


-(BOOL)isFirstImpression;

-(BOOL)isFirstAdCall;

-(void)updateFirstImpressionValue:(BOOL)value;

-(void)updateFirstAdCallValue:(BOOL)value;

@end
