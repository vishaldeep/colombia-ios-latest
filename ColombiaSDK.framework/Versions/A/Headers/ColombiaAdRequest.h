//
//  ColombiaAdRequest.h
//  ColombiaDemo
//
//  Created by Mohit Kumar on 10/27/15.
//  Copyright © 2015 Mohit Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ColombiaRequestProtocol.h"
#import "CLBBuilder.h"
@class CLBBuilder;

@interface ColombiaAdRequest:NSObject<ColombiaRequestProtocol>

@property (nonatomic,strong)CLBBuilder *builder;

-(NSMutableArray*)getAdRequests;
-(NSString*)getReferer;
-(NSMutableSet*)getAdManagerItems;
-(NSDate *)getAge;
-(ColombiaGENDER)getGender;
-(NSInteger)getResponseFormat;
-(NSString*)getLocation;
-(NSString *)getAdSlot;
-(NSInteger) getAdWidth;
-(NSInteger) getAdHeight;
-(NSArray *)getCustomAudiences;
-(id<CLBItemDelegate>)getDelegate;

-(NSNumber *)downloadAudio;
-(NSNumber *)downloadVideo;

@end
