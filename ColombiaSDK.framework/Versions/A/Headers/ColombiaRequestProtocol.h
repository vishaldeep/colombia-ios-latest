//
//  ColombiaRequestProtocol.h
//  ColombiaDemo
//
//  Created by Mohit Kumar on 10/29/15.
//  Copyright © 2015 Mohit Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ColombiaAdManager.h"

@protocol ColombiaRequestProtocol <NSObject>

-(NSMutableArray*)getAdRequests;
-(NSString*)getReferer;
-(NSMutableSet*)getAdManagerItems;
-(NSInteger)getAge;
-(ColombiaGENDER)getGender;
//-(NSArray*)getInstallAdApps;
-(NSString*)getLocation;
-(BOOL)isFirstAdCall;
-(BOOL)isFirstImpression;
-(void)updateFirstImpressionValue:(BOOL)value;
-(void)updateFirstAdCallValue:(BOOL)value;
@end
