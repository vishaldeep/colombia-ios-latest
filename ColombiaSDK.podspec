Pod::Spec.new do |s|
s.name             = "ColombiaSDK"
s.version          = "2.8.6"
s.summary          = "Colombia SDK for ads"

s.description      = <<-DESC
Colombia SDk to fetch ad from colombia network.
DESC

s.homepage         = "https://ads.colombiaonline.com/"
s.license          =  { :type => 'PRIVATE' }
s.author           = { "Mohit Kumar" => "mohit.kumar@timesinternet.com" }
s.source           = { :git=> "https://username@bitbucket.org/times_internet/colombia-ios.git",:tag => s.version}

s.platform     = :ios, '8.0'
s.requires_arc = true

s.resource_bundles = {
'ColombiaSDK' => ['Pod/Assets/*']
}

s.source_files = 'Pod/Classes/**/*','ColombiaSDK.framework/Versions/A/Headers/*.h'
s.vendored_frameworks = 'ColombiaSDK.framework'

s.dependency 'FBAudienceNetwork'
s.dependency 'Google-Mobile-Ads-SDK'

end
